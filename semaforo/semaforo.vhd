----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:51:31 11/17/2014 
-- Design Name: 
-- Module Name:    semaforo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity semaforo is
    Port ( Semaforo_1_Verde : out  STD_LOGIC;
           Semaforo_1_Ambar : out  STD_LOGIC;
           Semaforo_1_Rojo : out  STD_LOGIC;
           Semaforo_2_Verde : out  STD_LOGIC;
           Semaforo_2_Ambar : out  STD_LOGIC;
           Semaforo_2_Rojo : out  STD_LOGIC;
           Semaforo_Giro : out  STD_LOGIC);
end semaforo;

architecture Behavioral of semaforo is

begin


end Behavioral;

